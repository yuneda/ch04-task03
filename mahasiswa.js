const fs = require("fs");

function tambahMahasiswa(nama, asal) {
  const dataBuffer = fs.readFileSync("./data/mahasiswa.json", "utf-8");
  const data = JSON.parse(dataBuffer);
  const dataBaru = {
    id: data.length,
    nama,
    asal,
  };
  data.push(dataBaru);
  console.log(data);
  fs.writeFile("./data/mahasiswa.json", JSON.stringify(data), "utf-8", (err) => {
    if (err) throw err;
    console.log("Data Successfully Saved");
  });
}

tambahMahasiswa('Afifah', 'Tulangan');
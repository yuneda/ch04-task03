const express = require('express');
const mahasiswa = require("./data/mahasiswa.json");

const app = express();

app.get('/', (req, res) => {
    res.render('index.ejs')
})

app.get('/mahasiswa', (req, res) => {
    res.render('mahasiswa.ejs', {
        data: mahasiswa
    })
})

app.listen(3000);
